<?php


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */

if ( ! defined('MESMERIZE_THEME_REQUIRED_PHP_VERSION')) {
    define('MESMERIZE_THEME_REQUIRED_PHP_VERSION', '5.3.0');
}

add_action('init', 'register_shortcodes');
add_action('after_switch_theme', 'mesmerize_check_php_version');

function mesmerize_check_php_version()
{
    // Compare versions.
    if (version_compare(phpversion(), MESMERIZE_THEME_REQUIRED_PHP_VERSION, '<')) :
        // Theme not activated info message.
        add_action('admin_notices', 'mesmerize_php_version_notice');
        
        
        // Switch back to previous theme.
        switch_theme(get_option('theme_switched'));
        
        return false;
    endif;
}

function mesmerize_php_version_notice()
{
    ?>
    <div class="notice notice-alt notice-error notice-large">
        <h4><?php _e('Mesmerize theme activation failed!', 'mesmerize'); ?></h4>
        <p>
            <?php _e('You need to update your PHP version to use the <strong>Mesmerize</strong>.', 'mesmerize'); ?> <br/>
            <?php _e('Current php version is:', 'mesmerize') ?> <strong>
                <?php echo phpversion(); ?></strong>, <?php _e('and the minimum required version is ', 'mesmerize') ?>
            <strong><?php echo MESMERIZE_THEME_REQUIRED_PHP_VERSION; ?></strong>
        </p>
    </div>
    <?php
}

if (version_compare(phpversion(), MESMERIZE_THEME_REQUIRED_PHP_VERSION, '>=')) {
    require_once get_template_directory() . "/inc/functions.php";
    
     
    
    if ( ! mesmerize_can_show_cached_value("mesmerize_cached_kirki_style_mesmerize")) {
        
        if ( ! mesmerize_skip_customize_register()) {
            do_action("mesmerize_customize_register_options");
        }
    }
    
} else {
    add_action('admin_notices', 'mesmerize_php_version_notice');
}

//SHORTCODES

//1
function ourlocation(){
	?>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3232.886868124454!2d14.504334915117186!3d35.87628918014933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x130e5ac2d87c4891%3A0x48ee141582b02a26!2sMCAST!5e0!3m2!1sen!2smt!4v1589916780878!5m2!1sen!2smt" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	<?php
}

//2
function findourlocation(){
	return 'Our Headquarters are located in MCAST Paola Malta';
}

//3
function newproduct(){
	?>
	<iframe width="900" height="506" src="https://www.youtube.com/embed/NZPND7gJjK0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	<?php
}

//4
function imagesale() {
    return '<img src="https://macedonpubliclibrary.files.wordpress.com/2017/04/huge-sale.jpg?w=1140" width="750" height="750"/>';
	}
	
//5
function aboutus() {
	return 'Welcome to WP Shop, your number one source for all collectibles. We are dedicated to giving you the very best of our products, with a focus on dependability, customer service and uniqueness.
Founded in 2020 by Brandon Agius, WP Shop has come a long way from its beginnings in a garage. When Brandon first started out, his passion for collectibles drove him to quit his day job, and gave him the impetus to turn hard work and inspiration into to a booming online store. We now serve customers all over the world, and are thrilled to be a part of the fair trade wing of the collectibles industry.
<br/>
We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please do not hesitate to contact us.
<br/>
Sincerely,
Brandon';
}

//6
function imageaboutus() {
    return '<img src="https://images.vexels.com/media/users/3/161139/isolated/preview/5d043840a6eac4e1d56a945e5748b22e-thank-you-badge-sticker-by-vexels.png" width="400" height="400"/>';
	}
	
//7

function modeldesign() {
	?>
	<iframe src="https://app.modelo.io/embedded/gkKkG5PYyp?viewport=false&autoplay=false&c_at0=0.0010000169277191162&c_at1=1.1870999345555902&c_at2=-0.0658000260591507&c_theta=4.706687143782129&c_phi=1.570793185202243&c_dis=4.373268693502368" style="width:100%;height:100%;" frameborder="0" mozallowfullscreen webkitallowfullscreen allowfullscreen ></iframe> 
	<?php
}


function register_shortcodes(){
    add_shortcode( 'location' , 'ourlocation');
	add_shortcode( 'findlocation' , 'findourlocation');
	add_shortcode( 'newvideo' , 'newproduct');
	add_shortcode( 'sale' , 'imagesale');
	add_shortcode( 'about' , 'aboutus');
	add_shortcode( 'imageabout' , 'imageaboutus');
	add_shortcode( 'model', 'modeldesign');
	
}


